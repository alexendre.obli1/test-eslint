module.exports = {
    "env": {
        "browser": true,
        "es2021": true
    },
    "extends": "eslint:recommended",
    "parserOptions": {
        "ecmaVersion": "latest",
        "sourceType": "module"
    },
    "plugins": ["lintalexendre"],
    "rules": {
        "lintalexendre/no-get-core": 1,
        "semi": ["error", "always"],
        "quotes": ["error", "double"]
    },
    "extends": "eslint:recommended"
}