/**
 * @author Alexendre OBLI
 */
 "use strict";
 
 module.exports = {
     meta: {
         docs: {
            name: "no-get-core",
             description: "Vérification de l'emploi de getCore().byId()",
             category: "Error",
             recommended: true
         }
     },
 
     create: function (context) {
 
         const GET_CORE_ERROR_MESSAGE = "L'emploi de getCore().byId n'est pas autorisé";
 
/************************************************************************************************************
 *             ON CRÉAIT DANS UN PREMIER TEMPS LE MESSAGE QUI SERA RELEVÉ SI L'ÉRREUR APPARAIT              *
 * PUIS EN FONCTION DU NODE PARCOURU, SI L'ENSEMBLE DES CONDITIONS SONT RELEVÉES, ALORS ON AFFICHE L'ERREUR *
 ************************************************************************************************************/
 
         return {
 
             CallExpression(node) {
                 if (node.parent.property && node.parent.property?.name === "byId" && node.callee.property?.name === "getCore") {
                     context.report(node, GET_CORE_ERROR_MESSAGE);
                 }
             }
 
         };
     }
 };
