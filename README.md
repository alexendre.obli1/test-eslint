# eslint-plugin-lintalexendre

test linter

## Installation

You'll first need to install [ESLint](https://eslint.org/):

```sh
npm i eslint --save-dev
```

Next, install `eslint-plugin-lintalexendre`:

```sh
npm install eslint-plugin-lintalexendre --save-dev
```

## Usage

Add `lintalexendre` to the plugins section of your `.eslintrc` configuration file. You can omit the `eslint-plugin-` prefix:

```json
{
    "plugins": [
        "lintalexendre"
    ]
}
```


Then configure the rules you want to use under the rules section.

```json
{
    "rules": {
        "lintalexendre/rule-name": 2
    }
}
```

## Supported Rules

- no-get-core 


