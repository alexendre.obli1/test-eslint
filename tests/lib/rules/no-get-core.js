/**
 * @author Alexendre OBLI
 */
"use strict";
var rule = require("../../../lib/rules/no-get-core"),

    RuleTester = require("eslint").RuleTester;

var ruleTester = new RuleTester();
ruleTester.run("no-get-core", rule, {

    valid: [
        {
            code: 'var oView = this.getView();'
        }
    ],

    invalid: [
        {
            code: 'var oView = sap.ui.getCore().byId("maVuePrincipale");',
            errors: [{
                message: "L'emploi de getCore().byId n'est pas autorisé",
                type: "CallExpression"
            }]
        },
        {
            code: 'if(val){sap.ui.getCore().byId("maVuePrincipale").setValue(4);}',
            errors: [{
                message: "L'emploi de getCore().byId n'est pas autorisé",
                type: "CallExpression"
            }]
        }
    ]
});

