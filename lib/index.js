/**
 * @fileoverview test linter
 * @author alexendre
 */
"use strict";
//------------------------------------------------------------------------------
// Requirements

//------------------------------------------------------------------------------
// Plugin Definition
//------------------------------------------------------------------------------


// import all rules in lib/rules
module.exports = {
    rules : require("./rules"),
}


